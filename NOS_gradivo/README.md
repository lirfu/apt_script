#### Vrste napada:
1. Prisluškivanje
1. Prekidanje
1. Promjena sadržaja
1. Izmišljanje poruka
1. Lažno predstavljanje
1. Poricanje

#### Sigurnosni zahtjevi:
1. Tajnost (povjerljivost)
1. Raspoloživost
1. Besprijekornost
1. Autentičnost
1. Autorizacija
1. Neporecivost

#### Algoritmi za kriptiranje
| Naziv | Tip | Vel. ključa | Vel. bloka | Brzina
| --- | --- | ---: | ---: | ---:
| One Time Pad | simetrični | N | N | (XOR)
| DES | simetrični | 56b (7B) | 64b (8B) | 1e10 b/s
| 3DES | simetrični | 112b, 168b (2,3×56) (14B, 21B) | 64b | 3×DES
| DESX | simetrični | 184b (56+2×64) (23B) | 64b | ~DES
| IDEA | simetrični | 128b (16B) | 64b | DES / 2
| Galoiss field | simetrični | - | - | -
| AES (Rijandel) | simetrični | 128b (16B) | 128, 192, 256b | <DES

### DES
$C = DES( P,K )$

### 3DES
$C = DES( DES^{-1}( DES( P,K_1 ),K_2 ),K_3 )$

### DESX (izbijeljeni)
$C = DES( P\oplus K_2,K_1 )\oplus K_3$

### IDEA
 * Ključ se dijeli na 52 potključa 16b rotacijama
 * množenje po modulu, zbrajanje po modulu, XOR

### Galoiss field
* XOR i binarno množenje polinoma

### AES (Rijandel)
* operacije:
  * Zamijeni znakove - supstitucijska tablica
  * Posmakni redove - cilkira znakove 2., 3. i 4. reda
  * Pomiješaj stupce - prema principu Galoissovog polja (matrično množenje)
  * Dodaj potključ
* sve operacije imaju inverz
* broj koraka ovisi o veličini bloka podataka i veličini bloka ključa (broju stupaca, uvijek je 4 retka)

# Načini kriptiranja
## Elektronička bilježnica (ECB)
![ecb](ecb.png)
* *Electronic Notebook*
* kroptiranje neovisno o ostalim blokovima
* pogreška 1 bloka djeluje samo na taj blok

## Ulančavanje (CBC)
![cbc](cbc.png)
* *Cipher Block Chaining*
* blok ovisi o svim prethodnim blokovima
* pogreška 1 bloka djeluje na taj blok i sljedeći (prek-sljedeći je normalan)
* IV se šalje s porukom
* samoispravljajući - sam popravi grešku

![cbc_d](cbc_dec.png)

## CFB i OFB
![cbf](cbf.png)
* *Cipher Feedback* i *Output Feedback*
* za kriptiranje toka podataka
* ključ iste duljine kao podatak
* CFB pogreška 1 bloka djeluje na taj blok i sljedeći (prek-sljedeći je normalan)
* OFB pogreška na 1 blok djeluje samo na taj blok
* IV je javno poznat

## Brojač (CTR)
![ctr](ctr.png)
* *Counter*
* sličan OFB, samo se keystream dobiva kriptiranjem brojača


# RSA

Svojstva | Detalji
---|---
TM dijeljenja | $a = q\cdot n+r$
Relativno prosti brojevi | a i b nemaju zajedničkih faktora: $nzd(a,b)=1$
Eulerova Phi funkcija | $Z=\{0,1,...,n-1\}$<br>$Z'=\{a\in Z, nzd(a,n)=1\}$, (0 nije prost)<br>$\phi (n)=|Z'|$<br>$n=p\cdot q, \quad \phi(n)=n(1-\frac{1}{p})(1-\frac{1}{q})=(p-1)(q-1)$
Eulerov teorem | $a^{\phi(n)}=1(\mod n), \quad n>1$
Mali Fermatov teorem | $a^{p-1}=1(\mod p), \quad p-prosti\ broj$<br>$a^p=a(\mod p)$
Kineski TM ostatka | $n=p\cdot q$<br>$x=a(\mod n),\quad tada\ vrijede:$<br>$x=a(\mod p)$<br>$x=a(\mod q)$

#### Heurističko ispitivanje po Miller-Rabinu
$a^{p-1}=1(\mod p)$
* **Charmichaelovi brojevi** - nisu prosti i vrlo su rijetki, ali ih postupak smatra prostima

#### Rivest-Shamir-Adleman
* Postupak izgradnje:
  1. Dva velika prosta broja p i q
  2. $n=p\cdot q$
  3. $\phi(n)=(p-1)\cdot(q-1)$
  4. Odabire se $e<\phi(n),\quad nzd(e,\phi(n))=1$
  5. Izračuna se $d<\phi(n),\quad e\cdot d=k\cdot\phi(n)+1, \quad(e\cdot d=1(\mod \phi(n)))$
  6. Javni ključ: $K_E=(e,n)$
  7. Privatni ključ: $K_D=(d,n)$
* Kriptiranje: $\quad C=P^e\mod n$
* Dekriptiranje: $\quad P=C^d\mod n$

## Digitalna omotnica
* tajnost poruke (javni ključ), ali ne sprječava ostale napade (modifikacija, lažno predstavljanje)
* postupak:
  1. Ana kriptira podatak simetričnim: $C_1=DES(P,K)$
  2. Ana kriptira ključ Brankovim javnim: $C_2=RSA(K,K_{EB})$
  3. Ana šalje poruku: $M(C_1, C_2)$
  4. Branko privatnim ključem saznaje simetrični: $K=RSA^{-1}(C_1, K_{DB})$
  5. Branko simetričnim saznaje podatak: $P=DES^{-1}(C_2, K)$

## Digitalni potpis
* integritet poruke (sažetak) i autentičnost (javni ključ)
* postupak:
  1. Ana izračuna sažetak poruke: $H(P)=P_M\oplus(...\oplus(P_1\oplus P_0)), \quad P=P_0P_1...P_M$
  2. Ana kriptira sažetak svojim privatnim ključem: $C_H=RSA(H(P), K_{DA})$
  3. Ana šalje poruku sa sažetkom: $M=(P, C_H)$
  4. Branko Aninim javnim ključem dekriptira sažetak: $H(P)=RSA^{-1}(C_H, K_{EA})$
  5. Branko uspoređuje izračunati sažetak primljene poruke s dekriptiranim sažetkom

## Digitalni pečat
* potpisana poruka u digitalnoj omotnici
* postupak:
  1. Ana kriptira podatak simetričnim ključem: $C_1=DES(P,K)$
  2. Ana kriptira simetrični ključ Brankovim javnim: $C_2=RSA(K, K_{EB})$
  3. Ana izrađuje sažetak ključeva: $S=H(C_1, C_2)$
  4. Ana kriptira sažetak svojim privatnim ključem: $C_3=RSA(S, K_{DA})$
  5. Ana šalje poruku: $M=(C_1,C_2,C_3)$
  6. Branko dekriptira sažetak Aninim javnim: $S=RSA^{-1}(C_3, K_{EA})$
  7. Branko uspoređuje izračunati sažetak primljene poruke s dekriptiranim sažetkom
  8. Branko dekriptira sim. ključ svojim privatnim ključem: $K=RSA^{-1}(C_2, K_{DB})$
  9. Branko dekriptira podatak simetričnim: $P=DES^{-1}(C_1, K)$

# Funkcije sažimanja
#### Svojstva:
* Otpornost na izrač. originala - nema inverzne funkcije
* Otpornost na izrač. poruke koja daje isti sažetak - ne možeš naći poruku koja daje isti sažetak kao zadana poruka
* Otpornost na kolizije - ne možeš naći dvije poruke s istim sažetkom (lakše za probiti)

## MD5
* Message Digest
* sažetak: 128b
* ulazni blokovi: 512b
* nadopuna zadnjeg bloka:
  1. dodaj $1$
  2. popuni s $0$, osim zadnja 64b
  3. u 64b izvorna duljina poruke
* 16 podblokova
* 4 kruga, 16 koraka

## SHA
#### SHA-1
* sažetak: 160b
* ulazni blokovi: 512b
* nadopuna ista ko *MD5*
* sažetak: 5 podblokova sa 32b
* 4 kruga, 20 koraka
* +, I, ILI, XOR, rot
#### SHA-2
* sažetak: 224, 256, 384, 512
* ulazni blokovi: 512b
* sažetak: 16 podblokova po 32b
* +, I, ILI, XOR, rot, shift, zbrajanje po modulu
#### SHA-3
* spužvasta struktura, faza upijanja i cijeđenja

# Sigurnosni protokoli
### Diffie i Hellman razmjena ključa
1. Ana i Branko se dogovore oko 2 velika rel. prosta broja *p* i *g* (*p* je poželjno prost)
2. Ana izmisli veliki broj *x* i Branku pošalje rezultat:\
$X=g^x\ mod\ p$
3. Branko izmisli veliki broj *y* i Ani odgovori rezltat:\
$Y=g^y\ mod\ p$
4. Ana i Branko sada imaju simetrični ključ:\
$K=Y^x\ mod\ p = X^y\ mod\ p = g^{xy} \ mod\ p$

* osjetljiv na *man-in-the-middle* napade
  - napadač presretne *X* i *Y* te Ani i Branku pošalje svoj *Z* (oni misle da direktno komuniciraju)

## Zatvoreni simetrični kriptosustav
* ako svatko ima ključ za komunikaciju s ostalima, treba definirati $\frac{N(N-1)}{2}$ ključeva
* svatki korisnik čuva $N$ ključeva
### Needham i Schroeder (centralizirani)
![needham](needham.png)
* centar za raspodijelu ključeva (*Key Distribution Center*)
  - svakom korisniku dodijeli ID i ključ K
  - $ID_i$ je javan, $K_i$ ostaje tajan
* korisnik A želi komunicirati s B:
  1. A šalje bez enkripcije na KDC:\
  $M_1=(R_A, ID_A, ID_B),\quad$ $R_A$ je zahtjev za ključem
  2. KDC:
      - stvori $K_{AB}$
      - $C_1 = E((ID_A, K_{AB}),\ K_B)$
      - odgovori A porukom:\
      $M_2=E((R_A, ID_A,K_{AB}, C_1),\ K_A)$
  3. A dekriptira $M_2$ te:
      - na temelju $R_A$ i $ID_A$ zaključi da je odgovor na $M_1$
      - zapamti $K_{AB}$
      - pošalje B poruku:\
      $M_3=C_1$
  4. B dekriptira $C_1$ te:
      - pohranjuje $ID_A$ i $K_{AB}$
      - provjerava ima li A zajednički ključ:
        - slučajni broj R (nonce)
        - odgovori A porukom:\
        $M_4 = E(R,K_{AB})$
  5. A dekriptira $M_4$ i pošalje rezultat poznate funkcije F:\
  $M_5 = E(F(R), K_{AB})$
  6. B dekriptira poruku $M_5$ i provjerava $F^{-1}(F(R)) = R$
* mogućnost uvođenja vremenskog ograničenja na ključ:\
$C_1 = E((ID_A, K_{AB}, T_{AB}),\ K_B)$\
$M_2=E((R_A, ID_A,K_{AB}, C_1, T_{AB}),\ K_A)$
* ispad KDC-a onemogućava komunikaciju
* treba definirati $N$ ključeva
* svaki korisnik čuva $1$ (svoj) ključ, KDC čuva $N$ ključeva

### Raspodijeljena raspodjela ključeva
![raspodijeljeni](raspodijeljeni.png)
* korisnici raspoređeni u zatvorena područja, svaki sa svojim KDC
* KDC međusobno komuniciraju
* treba definirati $\frac{N(N-1)}{2}$ ključeva između KDC-a te $N_i$ između svakog KDC i njegovih korisnika
* svaki korisnik čuva $1$ (svoj) ključ, KDC čuva $(N-1)$ ključeva za ostale KDC te $N_i$ ključeva za korisnike
* ako su oba korisnika u istom području koristi se centralizirani algoritam
* inače:
  1. A šalje bez enkripcije na $KDC_i$:\
  $M_1=(R_A, ID_A, ID_B),\quad$ $R_A$ je zahtjev za ključem
  2. $KDC_i$:
      - stvori $K_{AB}$
      - odgovori A porukom:\
      $M_2=E((R_A, ID_A, K_{AB}),\ K_A)$
      - proslijedi zahtjev $KDC_j$ zaduženom za B:\
      $M_3=E((K_{AB}, ID_A, ID_B),\ K_j)$
  3. $KDC_j$:
      - dekriptira $M_2$
      - pošalje B poruku:\
      $M_4=E((K_{AB}, ID_A),\ K_B)$
    4. B dekriptira $M_4$ te:
        - pohranjuje $ID_A$ i $K_{AB}$
        - provjerava ima li A zajednički ključ:
          - slučajni broj R (nonce)
          - odgovori A porukom:\
          $M_5 = E(R,K_{AB})$
    5. A dekriptira $M_5$ i pošalje rezultat poznate funkcije F:\
    $M_6 = E(F(R), K_{AB})$
    6. B dekriptira poruku $M_6$ i provjerava $F^{-1}(F(R)) = R$

## Zatvoreni asimetrični kriptosustav
![asim](asim.png)
* PKM (*Public Key Manager*)
* PKM čuva svoj privatni ključ $K_{DC}$ i javne ključeve korisnika $K_{Ei}$
* korisnik čuva svoj privatni ključ $K_{Di}$ i javni PKM-a $K_{EC}$
* A želi kanal s B:
  1. A šalje PKM da sazna javni ključ B:\
  $M_1=E((R_A, T_1, ID_A, ID_B),\ K_{EC})$
  2. PKM odgovara A:\
  $M_2=((R_A, T_1, K_{EB}),   K_{EA})$
  3. A saznaje da je to odgovor na $M_1$ i šalje B slučajni broj $N_A$:\
  $M_3=((ID_A, N_A),   K_{EB})$
  4. B saznaje $ID_A$ i šalje PKM da sazna javni ključ A:\
  $M_4=((R_B, T_2, ID_A, ID_B),\ K_{EC})$
  5. PKM odgovara B:\
  $M_5=((R_B, T_2, K_{EA}), K_{EB})$
  6. B saznaje da je to odgovor na $M_4$ i šalje A njegov sl. broj i svoj sl. broj:\
  $M_6=((N_A, N_B),\ K_{EA})$
  7. A uspoređuje primljeni $N_A$ i odgovara B njegov:\
  $M_7=((N_B),\ K_{EB})$
  8. B uspoređuje primljeni $N_B$

# Autentifikacija zatvorenih sustava
* u gornjim sustavima se obavljala i autentifikacija
* centri se sada zovu AS (*Authentification Server*)

## Jednostrana autentifikacija u zat. sim. KS
![jednostrsim](jednostrsim.png)
* AS sadrži tajne ključeve i identifikatore
* A se javlja B:
  1. A šalje B svoj ID:\
  $M_1 = (ID_A)$
  2. B odgovara sl. brojem R:\
  $M_2 = (R)$
  3. A vraća kriptirani broj:\
  $M_3 = E(R,\ K_A)$
  4. B provjerava A na AS-u:\
  $M_4 = (ID_B, E((ID_A, E(R,\ K_A)),\ K_B))$
  5. AS dekriptira poruke pomoću ključeva za $ID_A$ i $ID_B$ i vraća R:\
  $M_5 = E(R, K_B)$
  6. B dekriptira $M_5$ i uspoređuje R

## Jednostrana autentifikacija u zat. asim. KS
![jednostrasim](jednostrasim.png)
* AS sadrži javne ključeve i identifikatore
* A se javlja B:
  1. A šalje B svoj ID:\
  $M_1 = (ID_A)$
  2. B odgovara sl. brojem R:\
  $M_2 = (R)$
  3. A vraća privatno kriptirani broj:\
  $M_3 = E(R,\ K_{DA})$
  4. B šalje AS zahtjev za javnim ključem A ($R_B$ je kod zahtjeva):\
  $M_4 = (ID_A, R_B)$
  5. AS odgovara privatno kriptirano:\
  $M_5 = E((ID_A, K_{EA}),\ K_{DC})$
  6. B dekriptira $M_4$, a zatim i $M_3$ te uspoređuje R

## Obostrana autentifikacija u zat. asim. KS
![obostrasim](obostrasim.png)
* obje strane vrše autentifikaciju
* 7 poruka
* dodjeli se i simetrični ključ K za kasnije simetrično kriptiranje
* postupak je LOL

# Autentifikacijski protokol Kerberos
![Kerberos](kerberos.png)
* pretpostavka da su računala u sustavu sigurna, mreža je nesigurna


# RAID
* Redundant Array of Independent Disks

## RAID-0
* zapisivanje po svim spremnicima
* $MTTDL=\frac{1}{N}MTTF$
## RAID-1
* zrcaljeno zapisivanje (svaki ima kopiju)
* $MTTDL=\frac{1}{N(N-1)}\cdot \frac{MTTF^2}{MTTR}+ \frac{2N-1}{N(N-1)}MTTF$
## RAID-2
* Hammingovo kodiranje, posebni diskovi za korelacijske bitove
* $MTTDL=\frac{1}{N(N-1)}\cdot \frac{MTTF^2}{MTTR}+ \frac{2N-1}{N(N-1)}MTTF$
## RAID-3
* sitna (bitovna) zrnatost
* paritetni disk čuva paritetnu zastavicu svakog pojasa (svih diskova) te ako znamo koji se (jedan) pokvario možemo popraviti
## RAID-4
* krupna zrnatost
* isto kao RAID-3 ali veći pojasevi
* pisanje je sporo (čitaj, čitaj, piši, piši)
## RAID-5
* krupna zrnatost
* paritetni pojasevi raspoređeni po svim diskovima
* $MTTDL=\frac{1}{N(N-1)}\cdot \frac{MTTF^2}{MTTR}+ \frac{2N-1}{N(N-1)}MTTF$
## RAID-6
* zaštita dvostrukog kvara
* K skupine po G diskova, ukupno diskova $N=K\cdot G$
* $MTTDL=\frac{1}{N(G-1)(G-2)}\cdot \frac{MTTF^3}{MTTR^2}$
