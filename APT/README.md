# Introduction

**Information retrieval** is finding material of an unstructured nature that satisfies an information need from within large collections.

**Text mining** is discovery of new information by automatically extracting information from written sources.
It differs from information retrieval because it discoveres **unknown** information.
It differs from data mining because it extracts patterns form natural patterns texts, instead from structured databases of facts.

Unstructured data doesn't have semantically overt structure. Example of this is natural language.
Text analysis is hard because NL is complex, ambiguous, vague and relies on common sense knowledge.

Ambiguity:
* categorial: *Time me on the last lap!*
* word sense: *He saw me there.*
* structural: *He was on the hill with the motorcycle.*
* referential: *Ann helped Lucy and she was happy again.*

NLP is a field of CS, AI and linguistics concerning interactions between computer and human languages.
NLP tasks:
* stemming and lemmatization
* language modeling and POST tagging
* parsing
* word sense disambiguation

NLP can be:
* rule-based: symbolic, logic based
* statistical: probability, statistics, ML

# Basics of NLP

1. Language detection
2. Text cleanup (OCR corrections, ...)
3. Sentence segmentation
4. Tokenization
5. Stemming
5. POS tagging
6. Lemmatization
7. Word-label semantics
8. Syntactic processing: parsing
9. Sentenct-level semantics
10. Discourse semantics and pragmatics

### Sentence segmentation
* finding sentences
* often using regex, best performance with supervised ML
### Tokenization
* breaking text into meaningful words
* rule based or with supervised ML
### Morphology
* inflectional - word forms with grammatical features
* derivational - new words from existing
* compounding - combine words into one
* e.g. searching *house* for housing or such, but not housewife
### Stemming
* suffix stripping and extra checks
* danger of over/understemming (*housing -> hous*)
* **Porter stemmer** - rule-based stemming, where a rule specifies the condition of the base and the transformation to the stemmed form
### Lemmatization
* transforming word to its linguistically valid base form (lemma)
* more complex than stemming
* for morphologically complex languages needs a morph. dictionary or ML model
### Part of speech (POS) tagging
* **Part of speech** explains how the word is used, not what it means (nouns(imenica), verbs, adjectives, ...)
* POS tagging _marks a word_ according corresponding to a part of speech (*A/**DT** boat/**NNP** is/**VBZ** ...*)
### Parsing
* builds a tree from the _grammatical structure_ of a given sentence


## Language model
Models describing the structures in language.
Used to determine:
* probability of a word sequence
* likely next word in a sequence
## Corpus
A large structured set of texts.
Used for training NL models (ML models).
## Zipf's law
Given a corpus, the frequency of any word is inversely
proportional to its rank in the frequency table.
This means that the most occuring words carry very little information compared to hapax legomena.
## WordNet
Manually constructed lexical database containing nouns, verbs, adjectives and adverbs, organized into similarity sets with descriptions.
E.g. *tire.n.01-hoop that coveres a wheel* or *tire.v.02.tire-exhaust or tired of overuse*.
Useful to NLP because it has a word hierarchy useful for semantics extraction.
## Semantic similarity/relatedness
Similarity describes do the words describe a similar object (*airplane-machine*), while relatedness describes are the words related to the same context(*airplane-pilot*).
## Word sense disambiguation (WSD)
**Polysemy** - a word has multiple related meanings (*wedding ring - boxing ring*).

WSD identifies the meaning of an ambiguous word.

# Basics of IR
Information need is a desire to locate and obtain some informatio to satisfy a need (usually a query).

## Text representation
### Unstructured (bag-of-words)
* oversimplification - ignores syntax and semantics
* good retrieval performance
* *"I go home now." -> [(I,1),(go,1),(home,1),(now,1)]"*
### Weakly-structured
* certaing groups of terms are given more importance (noun phrases, entity names, ...)
* *"I go home now." -> Nouns: [(I,1),(home,1)] Verbs: [(go,1)]  ..."*
### Structured
* not used in IR, not accurate, time costly

## Preprocessing
Reduces the cardinality of BOW -> performance boost
* **Morphological normalization** - putting the word into a common form (stemming and lematization)
* **Stop-words removal** - removing semantically poor terms (articles, prepositions, conjuctions, ...)

## Formalization of IR models
Each model consists of:
1. f_d - maps documents to their representations
2. f_q - maps queries to their representations
3. r - ranking function (relevance of document to the given query)

### Index terms (vocabulary)
* all the terms in the collection
* for each document a term has a weight (document is a vector of weights)

> Choice of weighing function and ranking function defines an IR model.

### IR paradigms
1. Set-theoretic models (boolean model)
2. Algebraic models (vector space model)
3. Probabilistic models (classic prob. model, language model)

## Boolean retrieval
* term weights are boolean (term is or isn't in the document)
* defined by document collection, vocabulary and query
* *(Frodo ∧ orc ∧ sword) ∨ (Frodo ∧ blue) ⇒ {d1, d2}*
##### Inverted file index
* each index term has list of documents containing it
##### Full inverted index
* each index term additionally contains the positions within documents\
##### Pros
* simplicity
##### Cons
* unintuitive querying, no ranking

## Vector space model
* documents and queries are vectors of weights (>=0) to index terms
* relavance is estimated by a vector similarity metric (Euclidian distance, Manhattan distance, Cosine similarity, Dice similarity)

### TF-IDF
* **tf** (term frequency) - proportional to the frequency of a term in a document
* **idf** (inverse term frequency) - inversly proportional to the number of documents in which the term occurs
> w_ij = tf(k_i, d_j) * idf(k_i, D) \* idf

## Probabilistic models
### Binary independence model
Has 2 assumptions:
* given relevance, terms are statistically independent
* presence of a term in a document depends on relevance only when that term is present in the query (rank is calculated only by terms in the query)
> p(D|q,r)=0.5 \
p(D|q,r')=n/N \
n - number of documents containing term
### Two Poisson model
* uses Poisson distribution to model frequencies
* assumes documents are of same size
* better than binary independence because it models the frequency of the term thus having more information to work with (binary ind. models only the existence of term)
* BM11 removes length assumptions (matches in longer documents less important), but too much dampening on long, too much boosting on short documents
* BM25 controls the document size correction (solves dampening/boosting) => SOA results

### Language modeling
* modeling query probability given a document (inverse probabilistic approach), probabilistic retrieval models document prob. given a query
* modeling probability of current word, based on all previous words 
* short documents disable usability of longer probabilities (data sparseness problem)
#### Unigram model
* simplify probability by eliminating dependence on previous words
> p(t) = n_t / N_t
#### Bigram model
* simplify probability by depending only in previous word
> p(t_2|t_1) = n(t_1, t_2) / N(t_1)
#### Smoothing
* previous probabilities give 0 to queries containing words not in the collection
* Laplace smoothing - adds small value to all (+unseen) word counts (assumes all unseen words equally likely)
* Jelinek-Mercer smoothing - adds small value to all, however the value varies on the likelihood of term in the whole collection

# Improved search
## Query expansion
Adding terms to user's query to improve results (synonyms or related words).

**Explicit expansion** - original query is used for search, expanded versions are suggested to user \
**Implicit expansion** - extended query is used for search

Determining good expansion words:
* Controlled vocabulary - terms from query mapped to cannonical terms (*{hotel, room} -> accomodation*) (human made)
* Manual thesaurus - human made sets of synonymous terms
* Auto-generated thesaurus - co-occurence statistics in large corpus automatically induse a thesaurus
* Query log mining - query reformulations tracked to suggest better expansions
 
## Relevance feedback (RF)
Improving results by using feedback from user.
1. Execute original q
2. Present results (monitor relevance from user)
3. Derive a refined q' (using user's feedback)
4. Execute q' and present final results to user

**Probabilistic retrieval** - relevance information is embedded into the probability estimation

**Rocchio's algorithm** - updated query consists of old query moved to be more similar to centroid of relevant documents and less similar to irrelevant

### Pseudo-relevance feedback
* RF without a human
* top results are assumed relevant
* better than query expansion
* danger of query drift (query focuses on other topics, getting biased)

## IR test collection
1. Document collection
2. Set of information needs (descriptions+queries)
3. Set of relevance judgements for each query-document pair

Accuracy (trues/all) is not a good metric because 99% results are irrelevant (search engine that returns nothing would be 99% accurate).

Common measures:
> P = TP / (TP+FP) \
R = TP / (TP+FN) \
F1 = 2PR / (P+R)

Mean average precision (MAP):
>![MAP](./MAP.png)
> where P(R) is: (*number of relevant up to including this*) / (*number of relevant docs*)

Precision at k (P@k):
> Precision of the top *k* ranked documents

R-Precision:
> Precision of the top *k* documents relevant to the query

**Popularity/importance** is introduced to doc. relevance because the Web is too large and filled with spam websites (difficulty defining relevance).

## PageRank
**PageRank hypothesis** - a page is important if it's pointed to by many other pages, that don't point to too many other pages.

Iterative matrix form: \
![matrix equation](./pr_matrix.png) \
*p(k)* - vector of PageRank scores in iteration *k*
*H* - adjecency matrix, row normalized \
*H_{ij}* - link weight from P_i to P_j (represents structure, not changed by algorithm)

Algorithm converges iff H has following adjustments:
* **Stochasticity** - surfer jumps from the *sink* to any other page with equal probability (eliminates 0 rows)
* **Primitivity** - surfer can at random time jump to any other page with equal probability (eliminates 0 cells)

This creates the google matrix (G):
![google equation](./pr_google.png)

## HITS (Hypertext induced search)
* **hub** - page containing many hyperlinks
* **authority** - page to which many pages point to
* page is a good hub if it points to good authorities
* page is a good authority if it is pointed to by good hubs

# Machine learning for NLP
Disadvantages of rule based NLP:
* too many rules needed
* difficult to mantain (new rules could break everything)
* need of expert knowledge (linguist)
* subjectivity problems

Advantages of ML:
* much easier to label data manually than to construct an algorithm that does it
* excludes subjectivity (data speaks for itself)
* labeled data might already be available
Divadvantages of ML:
* large quantities of data labeling could be expensive
* data labeling can be slow and error-prone
* models difficult to interpret (black-box)

Types of ML:
* supervised - labeled data for training (classification, ragression)
* unsupervised - no labeled data (clustering)

Classification - output is a discrete label \
Regression - output is a real (or int) value vector

Classification tasks:
* binary - one of two possible labels
* multiclass - one of *k* labels
* multi-label - multiple labels at once

Classifier training - optimizing the model parameters to minimize classification error \
Classifier testing - measuring the model's generalization on unseen data

Overfitting happens when the model maximally adapts to the given data, losing its generalization capabilities (small variance in inputs results in large output variance). \
Crossvalidation ensures that the model performance isn't biased by the train-test set selection (k-fold).

**Select - Train - Evaluate**:
1. Split dataset to train-test (70-30)
2. Test model with different hyperparameters (crossvalidation on the train set)
3. Select the best performing model
4. Train the model in entire train set
5. Evaluate trained model in test set

One-hot encoding is used for multiclass labels because some models expect real-valued outputs. Not a good idea encoding with one output from (0,K) because labels must be equidistant.

**Sequence labeling problem**:
* each token in sequence is assigned a label
* labels of tokens are dependent on labels of other tokens in sequence
* difficult to use surroundeing labels from both side of token
* doesn't account for uncertanty of token-wise decisions

### Hidden Markov model (HMM)
* assumption that the next state only depends on the current state and is independent of previous history
* finite state machine with probabilistic transitions

... TODO ...

# Text classification
Examples: spam filtering, document indexing (topic classification), genre class., autorship attribution, ...

Preprocessing:
* Tokenization
* Stop-words removal
* Morphological normalization (stemming or lemmatization)
* Feature selection

### Naive Bayes classifier
![](./nbc.png)

### Bernoullly classifier

### Multinomial classifier
... TODO whatever ...

Both models ignore the word position in document.
Bernoully ignores the word count, multinomial doesn't.

**Clustering** - statistical technique that allows automated generation of groupings in data

Latent document representation exploit the latent (hidden) structure and patterns the entire collection.
They're used for text analysis tasks (info. extraction, processing, visualization).

## Latent semantic analysis (LSA)
* construct matrix (words×documents)
* use SVD to find topic weights of each document
* leave only the k top topics => rank k approx.

## Latent Dirichlet allocation
